# Patterns

## Weighted hyphenation

The “hyphenation points” determined by the patterns in the files
`autotype-hyph-de-*.pat.txt` mark primary and secondary hyphenation points to
be used in a weighted hyphenation algorithm. The primary hyphenation points are
the best positions for hyphenation. These patterns have been created by means
of the German wortliste project (https://repo.or.cz/w/wortliste.git) using the
commands `make primary` and `make secondary`.

## Breaking up ligatures

The “hyphenation points” determined by the patterns in the file
`autotype-ligbreak-de.pat.txt` mark positions within a word where no ligature
should apply. These patterns have been created by means of the German wortliste
project (https://repo.or.cz/w/wortliste.git) using the following command:
`make de_ligaturaufbruch`.

## Long and round s

The “hyphenation points” determined by the patterns in the file
`autotype-round-s-de.pat.txt` mark positions within a word before which
a round s has to be used. These patterns have been created by means of the
German wortliste project (https://repo.or.cz/w/wortliste.git) using the
following command: `make schluss-s`.
