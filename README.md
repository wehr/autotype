# autotype

A lualatex package for automatic language-specific typography

## Patterns

The `patterns` directory contains pattern files having the same structure as
TeX hyphenation patterns, but serving for other purposes.

## Installation

Copy the files `autotype.lua`, `autotype.sty`, and `patterns/*.pat.txt` to a
place where TeX can find it. This should usually be `$TEXMFHOME/tex/lualatex/`.

## Commands

Load the package:
`\usepackage{autotype}`

Enable breaking up ligatures:
`\autotypelangoptions{⟨lang⟩}{ligbreak}`

`⟨lang⟩` is the language name of your language package (babel/polyglossia), e.g. `ngerman`.
Currently, only German and its varieties are supported.

Please read the manual `doc/autotype-de.pdf` for further information.

## Tests

The `tests` directory contains LaTeX test files for the package.
