# Tests

This directory contains the test files `weighted-hyph-Kant.tex` (traditional
orthography) for a weighted hyphenation algorithm, `break-ligatures.tex` for
breaking up ligatures and `insert-long-s.tex` for inserting long s in German
(traditional and modern orthography, also Swiss orthography). Compile them with
the `lualatex` engine.

The `words` subdirectory contains words which are relevant for the tests. Files
ending in `lig` or `nolig` contain words written with a certain letter
combination to which a ligature should apply or not apply, respectively. The
other files contain words written with a certain type of s (long or round) or a
combination of certain types of s. The lists have been created by means of the
German wortliste project (https://repo.or.cz/w/wortliste.git) using the
following commands:
```
./skripte/spezialmuster/ligaturwortauszug.sh wortliste
./skripte/spezialmuster/s-wort-auszug.sh wortliste
```
